# Doc Serveur Simplon Lyon

## Techno du serveur

* Ubuntu 16.04
* Caddy Server (HTTP)
   * Les configurations sont dans /etc/caddy/sites-enable
   * La CI crée un nouveau fichier de config par projet. Ces fichiers générés sont situés dans /etc/caddy/sites-enable/gitlab
* PHP FPM 7.2

## Mettre en ligne un projet

L'idée pour la mise en ligne sur simplonlyon.fr est de mettre le projet dans le groupe gitlab simplonlyon/site
où l'intégration continue gitlab (gitlab-ci) a été configurée pour s'exécuter directement dans
le shell du serveur.

1. Aller sur le projet à mettre en ligne et en faire un fork en choisissant comme destination simplonlyon/site/promo/username
2. Sur le fork, aller dans settings->CI / CD->Runners et cliquer sur `Disable shared runners`
3. Retourner à la racine du fork et cliquer sur `Set up CI / CD` qui vous fera créer un fichier .gitlab-ci.yml
4. Voici un exemple de configuration de base pour un projet symfony pdo
```
live:
  script:
    - cimplon server-conf symfony
    - cimplon init-sql db.sql
    - cimplon env-migrate pdo
    - composer.phar install
    - cimplon prod-mode
```
5. On peut indiquer à la suite les commandes à exécuter (ex: `npm install`, `php bin/console doctrine:migrations:migrate`, `npm run build`, etc.)
6. On peut vérifier dans le menu CI / CD > Jobs que le script s'est bien exécuté sans échec

Si échec ou problème, ouvrir une Issue sur ce dépôt où vous expliquerez le problème 
afin que je puisse le corriger et garder une trace pour les personnes qui aurait le même problème

### Points d'attention
* Dans le cadre d'un projet utilisant PDO, il FAUT que votre connexion à PDO utilise des variables d'environnement ayant comme noms : DATABASE_HOST DATABASE_USERNAME DATABASE_PASSWORD DATABASE_NAME comme dans [cet exemple](https://gitlab.com/simplonlyon/P9/symfony-pdo/blob/master/src/Repository/ConnectionUtil.php) ligne 35
* Dans les projets Symfony, tous les liens internes doivent être fait avec `path('...')`et toutes les images/css/vidéo internes aux projets doivent être chargée avec `asset('...')`
* Dans le cas où vous avez un fichier .sql qui crée votre base de données, ne pas mettre de CREATE DATABASE ou de USE dans ce fichier

## Références des commandes cimplon disponibles

### `server-conf <env>`
Script qui crée un nouveau fichier de configuration  Caddy Server pour l'apprenant.e si celui ci n'existe pas et rajoute la configuration pour le projet à l'intérieur.

Paramètres :
* `<env>` Pour indiquer le type de configuration serveur à créer (change selon le type de projet)
    * `symfony` Pour les projets Symfony, le serveur pointe sur le dossier public et applique l'url rewrite
    * `default`  Pour les projets php ou html/css classique, le serveur pointe sur la racine du projet
    * `webpack` Pour les projets webpack ou js, le serveur pointe sur le dossier dist
* Optionnels
  * `-c, --clear` Pour regénérer la configuration (par exemple si on a fait une erreur d'env)
  * `-p, --portfolio` Pour Indiquer que ce projet est le portfolio de l'apprenant.e

### `init-sql [db]`
Script qui crée une nouvelle base de données pour le projet.

Paramètres Optionnels :
* `[db]` Un fichier sql se trouvant dans le dépôt et qui sera importer à chaque exécution
* `-c, --clear` Commence par supprimer la base de donnée si celle ci existe

### `env-migrate <framework>`
Script qui crée un fichier d'environnement pour le projet.

Paramètres :
* `<framework>` Le frawmework dont il faut créer un fichier env
  * `doctrine` Pour faire un .env.local pour un projet Symfony 4.2 avec doctrine
  * `prevdoctrine` Pour faire un .env pour un project Symfony 4.0 ou 4.1 avec doctrine
  * `pdo` Pour faire un .env.local pour un projet Symfony 4.2 avec pdo

### `prod-mode`
Script qui crée un fichier d'environnement pour passer le projet en mode prod, à mettre à la fin des scripts