#!/usr/bin/env node

const fs = require('fs');
const sys = require('util')
const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const process = require('process');
const commander = require('commander');

const [match, promo, username, projectName] = process.env.CI_PROJECT_PATH.match(".*/(promo[0-9]*)/([a-z]*)/(.*)").map(item => item.toLowerCase());
// const [match, promo, username, projectName] = ['bloup', 'promo', 'username', 'projectname'];
const user = "user_" + process.env.GITLAB_USER_ID;
const projectId = process.env.CI_PROJECT_ID;
// const projectId = 100;
const projectDir = process.env.CI_PROJECT_DIR;
// const projectDir = '/folder/for/test';
const sqlConf = {
    username: username+promo,
    password: promo+username,
    host: process.env.DATABASE_HOST,
    project: username+promo+'_project_'+projectId
};

const rewriteSymfony = `
    rewrite {
        r .*
        ext /
        to /index.php?{query}
    }`;


commander.command('init-sql [file]')
    .description('Create database and optionnaly import sql file')
    .option('-c, --clear', 'Drop and recreate database for this project')
    .action(function (file, cmd) {
        if(!fs.readFileSync(__dirname+'/.mysql_users', 'utf8').includes(sqlConf.username)) {
            console.log('Creating SQL user...');
            fs.writeFileSync(__dirname+'/temp.sql', "GRANT ALL PRIVILEGES ON `"+sqlConf.username+"\\_%`.* TO '"+sqlConf.username+"'@'%' IDENTIFIED BY '"+sqlConf.password+"'");
            execSync(`mysql -u${process.env.DATABASE_USERNAME} -p${process.env.DATABASE_PASSWORD} < ${__dirname}/temp.sql`);
            fs.appendFileSync(__dirname+'/.mysql_users', sqlConf.username + ';');
            console.log('SQL user created !');
        }
        
        if (cmd.clear) {
            console.log('Dropping existing database if needed');
            execSync(`mysql -u${sqlConf.username} -p${sqlConf.password} -sse "DROP DATABASE IF EXISTS ${sqlConf.project};"`);
        }
        console.log('Creating database if needed');
        exec(`mysql -u${sqlConf.username} -p${sqlConf.password} -sse "CREATE DATABASE IF NOT EXISTS ${sqlConf.project};"`, function (err, out) {
            if (file) {
                console.log('Importing data from ' + file);
                exec(`mysql -u${sqlConf.username} -p${sqlConf.password} ${sqlConf.project} < ${file}`);
            }

        });
    });



commander
    .command('env-migrate <framework>')
    .description('Create environment file depending on the <framework>')
    .option('-p, --portfolio', 'Indicate that this project is your portfolio')
    .action(function (framework, cmd) {
        console.log(`Creating env file for ${framework}`);
        let projectUrl = `https://www.simplonlyon.fr/${promo}/${username}/${projectName}`;
        if(cmd.portfolio) {
            projectUrl = `https://www.simplonlyon.fr/${promo}/${username}`;
        }
        const envBase = `DATABASE_URL=mysql://${sqlConf.username}:${sqlConf.password}@${sqlConf.host}:3306/${sqlConf.project}
UPLOAD_DIRECTORY=${process.env.CI_PROJECT_DIR}/public/uploads
UPLOAD_URL=${projectUrl}
BASEURL=${projectUrl}
ROOT_PATH=${projectUrl}
JWT_SECRET_KEY=/home/gitlab-runner/.ssh/private.pem
JWT_PUBLIC_KEY=/home/gitlab-runner/.ssh/public.pem
JWT_PASSPHRASE=simplonserv
`;
        const envPDO = `DATABASE_USERNAME=${sqlConf.username}
DATABASE_PASSWORD=${sqlConf.password}
DATABASE_HOST=${sqlConf.host}
DATABASE_NAME=${sqlConf.project}
UPLOAD_DIRECTORY=${process.env.CI_PROJECT_DIR}/public/uploads
UPLOAD_URL=${projectUrl}`;
        const confVue = `module.exports = {
    publicPath: "/${promo}/${username}/" //TODO: OK pour version portfolio mais sinon non
}`;

        if (framework === 'doctrine') {
            fs.writeFileSync('.env.local', envBase);
        }
        if (framework === 'prevdoctrine') {
            fs.writeFileSync('.env', envBase);
        }
        if (framework === 'pdo') {
            fs.writeFileSync('.env.local', envPDO);
        }
        if (framework === 'vue') {
            fs.writeFileSync('vue.config.js', confVue);
        }
    });

commander.command('server-conf <env>')
    .description('Create a configuration file for Caddy depending on your project languages/frameworks')
    .option('-c, --clear', 'Recreate configuration for this project')
    .option('-p, --portfolio', 'Indicate that this project is your portfolio')
    .action(function (env, cmd) {
        const dir = `/etc/caddy/sites-enable/gitlab/${promo}`
        const confFile = `${dir}/${username}.conf`;

        let fileContent = '';
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        if (fs.existsSync(confFile)) {
            fileContent = fs.readFileSync(confFile, 'utf8');
        }

        let startToken = `### START Project ${projectId}`;
        let endToken = `### END Project ${projectId}`;
        if (cmd.portfolio) {
            startToken = `### START Project Portfolio`;
            endToken = `### END Project Portfolio`;
        }
        let newConf = '';

        if (env === 'symfony') {
            newConf = makeEnv(projectDir + '/public',
                rewriteSymfony, cmd.portfolio);
        } else if (env === 'webpack') {
            newConf = makeEnv(projectDir + '/dist', '', cmd.portfolio);
        } else {
            newConf = makeEnv(projectDir, '', cmd.portfolio);

        }



        if (fileContent.includes(startToken) && cmd.clear) {
            let start = fileContent.indexOf(startToken);
            let end = fileContent.indexOf(endToken);
            fileContent = fileContent.substring(0, start + startToken.length) + '\n' + newConf + '\n' + fileContent.substr(end);

        } else if (!fileContent.includes(startToken)) {
            fileContent += `
${startToken}
${newConf}
${endToken}`;
        } else {
            console.log('Configuration already exists');
            return;
        }


        if (fs.existsSync(`/etc/caddy/sites-enable/gitlab/project_${projectId}.conf`)) {
            console.log('Cleaning previous cimplon configuration file');
            fs.unlinkSync(`/etc/caddy/sites-enable/gitlab/project_${projectId}.conf`);

        }

        console.log('Writing Caddy configuration');

        fs.writeFileSync(confFile, fileContent, { flag: 'w' });
        exec('sudo systemctl reload caddy', function () {
            console.log('Reloaded Caddy');
        });

    });

commander.command('prod-mode')
    .description('Put your project in production mode')
    .action(function () {
        fs.writeFileSync('.env.dev.local', 'APP_ENV=prod', { flag: 'w' });
    });

commander.parse(process.argv);


function makeEnv(root, rewrite, portfolio) {
    let config = '';
    if (portfolio) {
        config += `www.simplonlyon.fr/${promo}/${username}/ {`;
    } else {
        config += `www.simplonlyon.fr/${promo}/${username}/${projectName}/ {`;
    }
    config += `
    root ${root}
    errors visible
    fastcgi / /run/php/php7.3-fpm.sock php

    ${rewrite}
}`;
    return config;
}